import hashlib

hostname = 'c1710475132'
username = 'root'
owner = 'root'
group = 'florian'
permissions = '0o4750'
swap = '4'
multithreading = 'multithreading (of course) is active'
sort_counter = 6
comment = 'not sure which hashtag you meant, so I added one: #pythondecompile'

solution_output = 'hostname: ' + hostname + '\nusername: ' + username + '\nowner: ' + owner + '\ngroup: ' + group + '\npermissions: ' + permissions + '\nSwap: ' + swap + '\n' + multithreading + '\nsorted: ' + str(sort_counter) + '\n' + comment + '\n'
solution = '@^@^@test@^@^@' + solution_output + '@^@^@test@^@^@'
hashcheck = hashlib.sha256(solution.encode('utf-8')).hexdigest()
print('------------------------------------------------------------------------------------')
print(solution_output + '\n' + hashcheck)
print('------------------------------------------------------------------------------------')